---
title: 自我介绍
date: 2019-11-29 10:45:33
type: "about"
---

### 简介

姓名：<strong>生一鸣</strong>

网站：https://yeemin.site 

邮箱：yeeminshon@outlook.com

毕业院校：上海第二工业大学 （2016届毕业生）

专业：计算机科学与技术（CDIO）

职业：Java开发工程师

<p id='resume_yeemin_wx'></p>

<script src="/js/resume.js"></script>

{% echarts 400 '100%' %}
{
     title: {
        text: '个人能力'
    },
    legend: {
        data: ['专业技能']
    },
    radar: {
        // shape: 'circle',
        indicator: [
            { name: 'Java', max: 100},
            { name: 'Spring', max: 100},
            { name: 'SQL', max: 100},
            { name: 'Redis', max: 100},
            { name: 'JavaScript', max: 100},
            { name: 'SpringBoot', max: 100},
            { name: 'Dubbo', max: 100}
        ]
    },
    series: [{
        name: '综合能力',
        type: 'radar',
        data: [
            {
                value: [90, 90, 85, 85, 80, 90, 80],
                name: '专业技能'
            }
        ]
    }]
};
{% endecharts %}





