---
title: 测试各种插件以及写法
date: 2021-06-25 00:20:20
tags:
    - 测试
    - 图片
categories: 测试分类
---

## 测试标题

下面是图片的显示
![](批注 2019-11-28 001956.jpg)

## 测试一下html标签

<a href="https://www.baidu.com">百度一下</a>

<ul>
    <li>a</li>
    <li>b</li>
    <li>c</li>
</ul>

<span style="color: RED;">还能设置样式</span>

## 测试js标签

<div id='test_js'></div>

<script>
document.getElementById("test_js").innerHTML = '<p>这句话是通过javascript控制显示的</p>';
</script>

<div id="vue_1">
{% raw %}{{ message }}{% endraw %}
</div>

<script>
var app = new Vue({
  el: '#vue_1',
  data: {
    message: '如果想使用Vue，由于Hexo会转义{% raw %}{{}}{% endraw %}，所以要用 \{\% raw \%\} \{\% endraw \%\}包围双大括号'
  }
})
</script>

<div id="app_6">
  <div>
  {% raw %}{{ message }}{% endraw %}
  </div>
  <input v-model="message">
</div>

<script>
var app6 = new Vue({
  el: '#app_6',
  data: {
    message: '摘自Vue官网Demo'
  }
})
</script>

