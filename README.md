# 个人博客

## 准备工作
> 先安装nodejs，版本号：12（大于此版本有问题）

**依次执行如下脚本**：
- npm install hexo-cli -g
- npm install hexo-asset-image --save 
- npm install hexo-generator-searchdb --save
- npm install hexo-deployer-git --save
- npm install hexo-tag-echarts --save --no-fund
- npm install --no-fund

## 运行脚本
- 清除编译：hexo clean
- 编译网站：hexo g
- 部署网站：hexo d

> 测试一下自动部署
